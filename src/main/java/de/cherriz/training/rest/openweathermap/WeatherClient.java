package de.cherriz.training.rest.openweathermap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class WeatherClient {

    private final HttpClient client;

    private final ObjectMapper mapper;

    private final String appId;

    private final String units;

    private final String lang;

    public static void main(String[] args) throws IOException, InterruptedException {
        String location = args[0];
        String appid = args[1];
        String units = args[2];
        String lang = args[3];

        WeatherClient client = new WeatherClient(appid, units, lang);
        String weather = client.getWeatherFor(location);

        System.out.println(weather);
    }

    public WeatherClient(String appId, String units, String lang) {
        this.appId = appId;
        this.lang = lang;
        this.units = units;
        this.client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
        this.mapper = new ObjectMapper();
    }

    public String getWeatherFor(String city) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(URI.create(
                "https://api.openweathermap.org/data/2.5/weather?appid=" + appId + "&lang=" + lang + "&units=" + units + "&q=" + city))
                                         .GET()
                                         .build();
        HttpResponse<String> response = this.client.send(request, HttpResponse.BodyHandlers.ofString());
        JsonNode node = mapper.readTree(response.body());

        double temperatur = node.path("main").path("temp").asDouble();
        String wetter = node.path("weather").get(0).path("description").asText();
        double wind = node.path("wind").path("speed").asDouble();
        return "In " + city + " hat es aktuell " + temperatur + "°C bei " + wetter + ".\nDie Windgeschwindigkeit liegt bei " + wind + "m/s.";
    }

}